#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 50

int isPalindrome(char str[], int beg, int end);


int main()
{
	char str[MAX_LEN] = { 0 };
	printf("Enter string: ");
	fgets(str, MAX_LEN, stdin);
	str[strlen(str) - 1] = '\0';
	if (isPalindrome(str, 0, strlen(str) - 1))
	{
		printf("Yes, it's a palindrome!");
	}
	else
	{
		printf("Not a palindrome!");
	}
	getchar();
	return 0;
}
/*
this function is checking if a string is a palindrome
input: a string, start index, end index
output: if it is or not
*/
int isPalindrome(char str[], int beg, int end)
{
	if (end <= 1)
	{
		return 1;
	}
	else
	{
		return str[beg] == str[end] && isPalindrome(str, beg + 1, end - 1);
	}
}